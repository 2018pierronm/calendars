package com.centralesupelec.calendars.calendars.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.centralesupelec.calendars.calendars.usefullObject.Association;
import com.centralesupelec.calendars.calendars.usefullObject.Happening;
import com.centralesupelec.calendars.calendars.R;
import com.centralesupelec.calendars.calendars.usefullObject.User;
import com.centralesupelec.calendars.calendars.database.AssociationsDAO;
import com.centralesupelec.calendars.calendars.database.HappeningDAO;

import java.util.Date;
import java.util.Set;
import java.util.Vector;

public class CalendarActivity extends AppCompatActivity implements View.OnClickListener {
    private Vector<Happening> vEvent;
    private Vector<TextView> vName;
    private Vector<TextView> vStart;
    private Vector<TextView> vEnd;
    private User me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        // Get the username from the shared preferences
        me = new User(getUsername());
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Ouverture des curseurs pour bases de données
        AssociationsDAO.open(this);
        HappeningDAO.open(this);

        // Load asso of interest from the shared preferences
        Vector<Integer> vIdAsso = this.loadAssoOfInterest();
        vEvent = new Vector<>();
        vEvent = this.loadHappening(vIdAsso);

        refreshDisplay();
    }

    /* Display conflict in red */
    public void updateConflict() {
        for (int i = 0; i < vEvent.size(); i++) {
            // Check conflict with ALL previous events
            int j = 1;
            while (i-j >= 0) {
                if (vEvent.get(i - j).getEndDate().after(vEvent.get(i).getStartDate())) {
                    vStart.get(i).setTextColor(Color.RED);
                    vEnd.get(i-j).setTextColor(Color.RED);
                }
                j++;
            }
        }
    }


    /* Display a card for each event */
    public void refreshDisplay() {
        LinearLayout ll = findViewById(R.id.calLayout );
        // Remove all old cards
        ll.removeAllViewsInLayout();

        vName = new Vector<>();
        vStart = new Vector<>();
        vEnd = new Vector<>();
        for (Happening ev:vEvent) {
            // Create all objects
            CardView card = (new CardView(this));
            TextView tvName = new TextView(this);
            TextView tvAsso = new TextView(this);
            TextView tvStart = new TextView(this);
            TextView tvEnd = new TextView(this);
            CheckBox cb = new CheckBox(this);
            LinearLayout cardLayout = new LinearLayout(this);
            LinearLayout dateLayout = new LinearLayout(this);

            // Text view for asso name
            tvAsso.setText(ev.getAssociation().getName());
            tvAsso.setTextSize(20);
            tvAsso.setTypeface(null, Typeface.BOLD);

            // Text view for event name
            tvName.setText(ev.getName());
            tvName.setTextSize(20);
            // Occupy maximum space and push the rest to the right
            LinearLayout.LayoutParams lParam = new LinearLayout.LayoutParams(0, 100);
            lParam.setMargins(30,0,10,0);
            lParam.weight = 1;
            tvName.setLayoutParams(lParam);

            // Text views for formated start/end date
            android.text.format.DateFormat df = new android.text.format.DateFormat();
            String start = df.format("dd/MM hh:mm", ev.getStartDate()).toString();
            String end = df.format("dd/MM hh:mm", ev.getEndDate()).toString();
            tvStart.setText(start);
            tvEnd.setText(end);

            // Checkbox
            cb.setTag(ev);
            // Check the event if we already said we attend
            // TODO le back ne suit pas encore pour ca : il faut push les changement vers la db
            if (ev.getParticipants().contains(me))
                cb.setChecked(true);
            cb.setOnClickListener(new View.OnClickListener() {
                // Update la liste des participant a chaque changement
                @Override
                public void onClick(View v) {
                    CheckBox box = (CheckBox) v;
                    Happening event = (Happening) box.getTag();
                    if (box.isChecked())
                        event.addParticipant(me);
                    else {
                        event.removeParticipant(me);
                        Log.i("salade", "remove "+me.getName());
                    }

                }
            });


            // Layout from date
            dateLayout.setOrientation(LinearLayout.VERTICAL);
            dateLayout.addView(tvStart);
            dateLayout.addView(tvEnd);

            // Layout from card
            cardLayout.addView(tvAsso);
            cardLayout.addView(tvName);
            cardLayout.addView(dateLayout);
            cardLayout.addView(cb);

            // Card setting
            card.addView(cardLayout);
            card.setMaxCardElevation(8);
            card.setUseCompatPadding(true);
            card.setPadding(100, 50, 25, 25);
            card.setClickable(true);
            card.setOnClickListener(this);
            card.setTag(R.id.attendee_tag, ev.getParticipants());
            card.setTag(R.id.desc_tag, ev.getDescription());

            // Add the card to main layout
            ll.addView(card);

            // Add the text view to corresponding vector
            vName.add(tvName);
            vStart.add(tvStart);
            vEnd.add(tvEnd);
        }

        // If there is no event to print
        if (vEvent.size() == 0) {
            TextView nothingToShow = new TextView(this);
            nothingToShow.setText(getString(R.string.noevent));
            nothingToShow.setTextSize(20);
            ll.addView(nothingToShow);

        }
        // Add white space to allow scrolling to the end
        TextView tvSpace = new TextView(this);
        tvSpace.setHeight(250);
        ll.addView(tvSpace);
        // Add color on date to display conflict
        updateConflict();
    }


    /* return the username, saved in a sharedPref */
    public String getUsername() {
        SharedPreferences sp = getSharedPreferences("data", Context.MODE_PRIVATE);
        String username = sp.getString("Username", null);
        return username;
    }

    /* return a vector of id from the suscribed associations, saved in a sharedPref */
    public Vector<Integer> loadAssoOfInterest() {
        SharedPreferences sp = getSharedPreferences("data", Context.MODE_PRIVATE);
        Set<String> sIdAsso = sp.getStringSet("idAsso", null);

        Vector<Integer> vIdAsso = new Vector<Integer>();

        if (sIdAsso != null){
            for (String s:sIdAsso) {
                vIdAsso.add(Integer.parseInt(s));
            }
        }
        return vIdAsso;
    }

    /* return a vector of events, concerning suscribed associations */
    public Vector<Happening> loadHappening(Vector<Integer> vIdAsso) {
        /*
        Requete :
            * assos preselectionnees ds vIdAsso seulement
            * event pas encore passé
            * triee par date croissante */
        // On crée un curseur issue d'une requête donnée si dessus
        Cursor cursor =HappeningDAO.loadHappeningCalendar(vIdAsso);
        Vector<Happening> vHapp = new Vector<>();
        if (cursor != null){

            while (cursor.moveToNext()){
                int id = cursor.getInt(HappeningDAO.ID_ASSOCIATION);
                String name =cursor.getString(HappeningDAO.ID_NOM);
                String description = cursor.getString(HappeningDAO.ID_DESCRIPTION);
                Date start =new Date(cursor.getLong(HappeningDAO.ID_START));
                Date end = new Date(cursor.getLong(HappeningDAO.ID_END));
                int id_asso = cursor.getInt(HappeningDAO.ID_ASSOCIATION);
                Association association =new Association(id_asso,AssociationsDAO.getNameFromId(id_asso));

                vHapp.add(new Happening(id,name,description,start,end,association));
            }
        }
        /*
        // Events test
        Association viarezo = new Association(1, "Viarezo");
        Date hd1 = new Date(2018,12,1,20,0);
        Date hf1 = new Date(2018,12,1,21,0);
        Happening event1 = new Happening("reunion vr", "desc", hd1, hf1, viarezo);

        Association guilde = new Association(2, "La Guilde");
        Date hd2 = new Date(2018,12,4,19,15);
        Date hf2 = new Date(2018,12,4,21,0);
        Happening event2 = new Happening("reunion ndt", "desc", hd2, hf2, guilde);

        Association symposium = new Association(3,"Symposium");
        Date hd4 = new Date(2018,12,4,19,15);
        Date hf4 = new Date(2018,12,4,19,30);
        Happening event4 = new Happening("reunion padr", "desc", hd4, hf4,symposium);

        Date hd3 = new Date(2018,12,4,20,30);
        Date hf3 = new Date(2018,12,4,21,30);
        Happening event3 = new Happening("tracos", "desc", hd3, hf3,symposium);
        event3.addParticipant(new User("bob"));


        Vector<Happening> vHapp = new Vector<Happening>();
        vHapp.add(event1);
        vHapp.add(event2);
        vHapp.add(event4);
        vHapp.add(event3);

        */
        return vHapp;
    }


    /* Si on clic sur un event, on voit la description et les participants */
    @Override
    public void onClick(View v) {
        Intent toAttendeeAct = new Intent();
        toAttendeeAct.setClass(this, AttendeeActivity.class);
        toAttendeeAct.putExtra("attendee", (Vector<User>) v.getTag(R.id.attendee_tag));
        toAttendeeAct.putExtra("desc", (String) v.getTag(R.id.desc_tag));
        startActivity(toAttendeeAct);
    }

    /* Changement d'activite pour ajouter un event */
    public void goToAdd(View view) {
        Intent toAdd = new Intent();
        toAdd.setClass(this, AddActivity.class);
        startActivity(toAdd);
    }

    /* Ferme les bases des qu'on change d'acti, pour permettre leur utilisation au autres actis */
    @Override
    protected void onPause() {
        AssociationsDAO.close();
        HappeningDAO.close();

        super.onPause();
    }

    /* Changement d'activite pour choisir ses assos */
    public void goToSetting(View view) {
        Intent toSet = new Intent();
        toSet.setClass(this, SelectActivity.class);
        startActivity(toSet);
    }
}
