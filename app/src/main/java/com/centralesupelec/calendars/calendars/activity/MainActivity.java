package com.centralesupelec.calendars.calendars.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.centralesupelec.calendars.calendars.usefullObject.Association;
import com.centralesupelec.calendars.calendars.usefullObject.Happening;
import com.centralesupelec.calendars.calendars.R;
import com.centralesupelec.calendars.calendars.database.AssociationsDAO;
import com.centralesupelec.calendars.calendars.database.HappeningDAO;
import com.centralesupelec.calendars.calendars.usefullObject.Happening;

import java.util.Date;

public class MainActivity extends AppCompatActivity {

    /**
     * On regarde si la base de donnée est générée : si oui on passe, si non on la génère.
     *
     * On vérifie si le fichier avec le pseudo existe : si ce n'est pas le cas on ouvre l'activité
     * pour demander le pseudo, sinon on dit que tout est bon.
     *
     * On passe toutes les assos dans l'activité suivante.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AssociationsDAO.open(this);
        HappeningDAO.open(this);
        //si on ne compte aucune ligne on doit remplir la table
        if (AssociationsDAO.countAll()==0){
            remplirTable();
        }

        //Si l'utilisateur a déjà donné son pseudo : on le log directement sur le calendrier.
        SharedPreferences sp = getSharedPreferences("data", Context.MODE_PRIVATE);
        Intent intent;
        if (sp.contains("Username")){
            intent = new Intent(this, CalendarActivity.class) ;
        } else {
            intent = new Intent(this, LoginActivity.class);
        }
        startActivity(intent);
        AssociationsDAO.close();
        HappeningDAO.close();
        finish();
    }

    /**
     * Remplie la table avec quelques assos si elles est vide : cela sert pour la démonstration
     */
    private void remplirTable(){
        Association viarezo = new Association("Viarezo");
        Association guilde = new Association("La Guilde");
        Association symposium = new Association("Symposium");



        Date hd1 = new Date(2018,12,1,20,0);
        Date hf1 = new Date(2018,12,1,21,0);
        Happening event1 = new Happening("reunion vr", "desc", hd1, hf1, viarezo);

        Date hd2 = new Date(2018,12,4,19,15);
        Date hf2 = new Date(2018,12,4,21,0);
        Happening event2 = new Happening("reunion ndt", "desc", hd2, hf2, guilde);

        Date hd4 = new Date(2018,12,4,19,15);
        Date hf4 = new Date(2018,12,4,19,30);
        Happening event4 = new Happening("reunion padr", "desc", hd4, hf4,symposium);

        Date hd3 = new Date(2018,12,4,20,30);
        Date hf3 = new Date(2018,12,4,21,30);
        Happening event3 = new Happening("tracos", "desc", hd3, hf3,symposium);


    }
}
