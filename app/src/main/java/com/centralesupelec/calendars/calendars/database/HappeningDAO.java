package com.centralesupelec.calendars.calendars.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.centralesupelec.calendars.calendars.usefullObject.Association;

import java.util.Date;
import java.util.Vector;

/**
 *
 */

public class HappeningDAO {
    public static final String TABLE_NAME = "Happenings";
    public static final String KEY = "_id";
    public static final int ID_KEY = 0;
    public static final String NOM ="nom";
    public static final int ID_NOM = 1;
    public static final String DESCRIPTION ="description";
    public static final int ID_DESCRIPTION = 2;
    public static final String START ="start";
    public static final int ID_START = 3;
    public static final String END ="end";
    public static final int ID_END =4;
    public static final String ASSOCIATION ="association";
    public static final int ID_ASSOCIATION = 5;

    public static SQLiteDatabase db;


    public static final String[] COLUMNS ={KEY,NOM,DESCRIPTION,START,END, ASSOCIATION};

    // On créer la requete de création de la table
    public static final String TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +" ( "
            + KEY +" INTEGER PRIMARY KEY, "+ NOM + " TEXT, "+DESCRIPTION+" TEXT, " +
            START+" INTEGER, "+ END +" INTEGER, "+ASSOCIATION+" INTEGER);";


    /**
     * A utiliser au début d'une activité: permet d'ouvrir la BDD
     * @param context
     */
    public static void open(Context context) throws SQLiteException {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException ex) {
            db = dbHelper.getWritableDatabase();
        }
    }


    /**
     * Renvoie un curseur avec toute la table
     * @return cursor
     */
    public static Cursor readAll(){
        Cursor cursor = db.query(TABLE_NAME,COLUMNS,null,null,null,null,null,null);
        return cursor;
    }


    /**
     * On récupère le nom de l'event et on l'ajoute à la base de donnée : on récupère son id
     * renvoit -1 si l'insertion est un échec
     * @return id
     */
    public static long insertValue(String name, String description, Date start, Date end,Association association){
        ContentValues values = new ContentValues();
        values.put(NOM,name);
        values.put(DESCRIPTION,description);
        values.put(START,start.getTime());
        values.put(END,end.getTime());
        values.put(ASSOCIATION, association.getId());
        long result = db.insert(TABLE_NAME,null,values);
        return result;
    }

    public static int countAll(){
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM "+TABLE_NAME,null);
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public static void close(){
        db.close();
    }

    /**
     * Permet de récupérer tous les évènements associés aux asso dont on a donné l'id dans le vecteur
     * @param vIdAsso
     * @return
     */
    public static Cursor loadHappeningCalendar(Vector<Integer> vIdAsso) {
        Cursor cursor;
        if (vIdAsso.size() > 0) {
            //On récupère la date et on ne regarde que les évènements présents et futurs
            long date = (new Date()).getTime();


            // On forge la question
            String selection = END + " >= ? AND ( "+ ASSOCIATION + " = ? ";
            //String selection = ASSOCIATION + " = ? ";
            for (int i = 0; i < vIdAsso.size()-1; i++) {
                selection = selection + " OR " + ASSOCIATION + " = ? ";
            }
            selection = selection + " ) ";
            //On lui donne les arguments : d'abord la date puis les id
            String[] selectionArgs = new String[vIdAsso.size()+1];
            selectionArgs[0] = Long.toString(date);
            for (int i = 0; i < vIdAsso.size(); i++) {

                selectionArgs[i+1] = vIdAsso.get(i).toString();
            }

            String order = START;

            cursor = db.query(TABLE_NAME, COLUMNS, selection, selectionArgs, null, null, order);
        } else {
            cursor = null;
        }
        return cursor;
    }

}
