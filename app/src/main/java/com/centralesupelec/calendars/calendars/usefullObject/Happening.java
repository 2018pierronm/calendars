package com.centralesupelec.calendars.calendars.usefullObject;

import com.centralesupelec.calendars.calendars.database.HappeningDAO;

import java.util.Date;
import java.util.Vector;

public class Happening {

    private int id;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private Association association;
    private Vector<User> participants;

    public Happening(String name, String description, Date startDate, Date endDate, Association association) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.association = association;
        this.id = (int) HappeningDAO.insertValue(name, description,startDate,endDate, association);
        this.participants = new Vector<>();
    }

    public Happening(int id, String name, String description, Date startDate, Date endDate, Association association) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.association = association;
        this.participants = new Vector<>();
    }

    Happening(String name, String description, Date startDate, Date endDate, Vector<User> participants) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.participants = participants;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void addParticipant(User p) {
        if (!participants.contains(p)) {
            this.participants.add(p);
        }
    }

    public void removeParticipant(User p) {
        this.participants.remove(p);
    }

    public Vector<User> getParticipants() {
        return this.participants;
    }

    public Association getAssociation() {
        return association;
    }
}
