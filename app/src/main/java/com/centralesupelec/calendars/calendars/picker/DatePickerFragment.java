package com.centralesupelec.calendars.calendars.picker;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.centralesupelec.calendars.calendars.activity.AddActivity;
import com.centralesupelec.calendars.calendars.R;

import java.util.Calendar;

/**
 * Classe permettant l'affichage d'un calendrier pour la sélection de la date de l'événement.
 * */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //On utilise les valeurs actuelles comme date par défaut.
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        //On affiche un Toast pour demander la date
        Toast.makeText(getContext(), getString(R.string.choose_date), Toast.LENGTH_SHORT).show();

        //On ferme le clavier
        InputMethodManager imm = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

        //On renvoit une nouvelle instance de DatePicker
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    /**
     * Fonction appelée lorsque la sélection de la date a été faite.
     * */
    public void onDateSet(DatePicker view, int year, int month, int day) {

        //On récupère l'EditText correspondant à l'entrée de la date.
        EditText et = getActivity().findViewById(R.id.start_date);

        //On rajoute une 0 devant le jour s'il est inférieur à 10
        String day_str = (day < 10) ? "0" + day : Integer.toString(day);

        //On rajoute une 0 devant le numéro du mois s'il est inférieur à 10
        String month_str = (month < 10) ? "0" + (month + 1) : Integer.toString(month + 1);

        //On met la valeur obtenue dans la zone correspondant à la date
        et.setText(String.format(getString(R.string.date_format), day_str, month_str, year));

        //On passe au  choix de l'heure de début
        ((AddActivity) getActivity()).showTimePickerDialog(view);

        //On appelle la fonction de vérification des champs
        ((AddActivity) getActivity()).checkFields();
    }
}