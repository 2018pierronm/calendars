# CalendarS

1) Présentation
Une application mobile pour gérer le calendrier associatif : on peux
    - suscribe à des associations
    - voir leur événement (réunion, etc...)
    - créer des événements
    - repérer des conflits entre plusieurs événements

2) Crédits
ARMILLON Damien  : background (DAO et database)
BRUCKMANN Nathan : Activités Calendar et Attendee
PIERRON Mathis   : Activité Add; création d'un serveur local
PROUST Florentin : Activité Select; traduction

Icon made by Freepik from www.flaticon.com